# Bases de dades (M02)
---

Hores|Unitat formativa|Data Inici|Data fi
-|-|-|-
25|UF1. Introducció a les bases de dades (Disseny BBDD)| 20/9/2023 | 30/10/2023  
58|UF2-Llenguatge SQL: DDL i DML | 3/11/2023 | 9/2/2024  
49|UF3-Assegurament de la informació | 14/2/2024 | 22/3/2024  

# Bases de dades (M10)
Hores|Unitat formativa|Data Inici|Data fi
-|-|-|-
66|Llenguatges SQL: DCL i extensió procedimental. SGBD corporatiu| 3/04/2024 | 31/05/2024  
---

[Motores de bases de datos] (https://db-engines.com/en/ranking)

## Base de datos
Una base de datos es un “almacén" que nos permite guardar (en un soporte no
volatil), grandes cantidades de información de manera organizada, de forma que
posteriormente podamos acceder a ella para que luego la utilicemos
(explotación de datos a través de data warehouse Y BI) para fines comerciales o no.  

Una base de datos se suele basar en un ```modelo```, aunque hay bases de datos multimodelo.

Tipos de modelos:
- jerárquico
- de red
- relacional
- lógico (deductivo)
- orientado a objetos
- documental
- de clave-valor
- multivalor
- tabular
- de grafo
- de arrays
- ...

Para crear una base de datos, almacenar datos, recuperarlos, manipularlos, una
base de datos necesita (en la mayoria de los casos) un ```sistema gestor```.  

En función de cómo se accede a los datos, podremos tener bases de datos de dos tipos:
- planas
- binarias

El acceso a una base de datos se podrá hacer sin más en el caso de una base de
datos plana pero en la mayoría de los casos se necesitará una herramienta
informática, un SGBD.

Si se busca definición de base de datos, muchas veces aparecerá en ella el
adjetivo "datos relacionados", porque sin especificarlo, se esta definiendo una
base de datos "relacional", basada en el ```modelo relacional``` (el más popular y
extendido).  

El modelo (de administración de datos) marca cómo se organiza la información y
condiciona la forma de manipular los datos.  

### Habitatges  

Dissenyar un esquema E/R que reculli l'organització d'un sistema d'informació en el qual es vol tenir la informació sobre municipis, habitatges i persones. Cada persona només pot habitar en un habitatge, però pot ser propietària de més d'una. També ens interessa la interrelació de les persones amb el seu cap de família.
Feu els supòsits semàntics (`requeriments d'usuari`) complementaris necessaris.  

---  

### Gabinet d'advocats  

Es vol dissenyar una base de dades relacional per a emmagatzemar informació
sobre els assumptes que porta un gabinet d'advocats.
- Cada assumpte té un número d'expedient que l'identifica i correspon a un sol
client.
- De l'assumpte s'ha d'emmagatzemar la data d'inici, data d'arxiu (finalització)
, el seu estat (en tràmit, arxivat, etc), així com les dades personals del
client al qual pertany (DNI, nom, adreça, telèfon).
- Alguns assumptes són portats per un o diversos procuradors i viceversa, dels
quals ens interessa també les dades personals (DNI, nom, adreça, telèfon).  

---
### Zoos  
Es vol dissenyar una base de dades relacional per emmagatzemar informació relativa als zoos existents en el món, així com les espècies i animals que aquests alberguen.
- De cada zoo es vol emmagatzemar el seu codi, nom, la ciutat i país on es troba, mida (m2) i pressupost anual.
- Cada zoo codifica els seus animals amb un codi propi, de manera que entre zoos es pot donar el cas que es repeteixi.
- De cada espècie animal s'emmagatzema un codi d'espècie, el nom vulgar, el nom científic, família a la qual pertany i si es troba en perill d'extinció.
- A més, s'ha de guardar informació sobre cada animal que els zoos tenen, com el seu número d'identificació, espècie, sexe, any de naixement, país d'origen i continent.

---

### Carreteres  

Dissenyar una base de dades que contingui informació relativa a totes les carreteres d'un determinat país. Es demana realitzar el disseny en el model E/R, sabent que:

En aquest paıs les carreteres es troben dividides en trams.
Un tram sempre pertany a una única carretera i no pot canviar de carretera.
Un tram pot passar per diversos termes municipals, sent una dada d'interès al km. del tram pel qual entra en dit terme municipal i al km. pel qual surt.
Hi ha una sèrie d'àrees en les que s'agrupen els trams, cada un dels quals no pot pertànyer a més d'una àrea.

Establiu els atributs que considereu oportuns i d'ells, trieu indentificador per a cada entitat.

---

### Càtedres d'Universitat  

Dissenyar una base de dades que reculli l'organització d'una Universitat. Es considera que:

Els departaments poden estar en una sola facultat o ser interfacultatius.
Una càtedra es troba en un únic departament.
Una càtedra pertany a una sola facultat.
Un professor està sempre assignat a un únic departament i adscrit a una o diverses càtedres, podent canviar de càtedra, però no de departament. Interessa la data en què un professor és adscrit a una càtedra.
Hi ha àrees de coneixement, i tot departament tindrà una única àrea de coneixement.

---  

### Museus  

Una persona amant de l'art vol construir una base de dades de museus del món i les obres d'art de cadascú. Per les limitacions del seu equip informàtic (i les seves pròpies) va considerar únicament pintures i escultures.

Per la informació de què disposa, pot saber en quin museu està cada obra d'art i, a més es coneix la sala del museu en la qual està l'obra. Les sales dels museus tenen un nom i pot haver sales amb mateix nom en diferents museus.
Com a aficionat a la matèria que és, sap que tota obra d'art té un títol.
Altres dades són específics del tipus d'obra d'art que consideri: pintura o escultura. Així, de les pintures es coneix el seu format de anchoxalto i el tipus de pintura (oli, pastel, aquarel·la, ...). De les escultures es considera el material amb què estan fetes (bronze, ferro, marbre, ...) i l'estil de l'escultura (neoclàssica, grecoromana, cubista, ...).
Li interessarà també, conèixer els autors de les obres. Les dades generals dels autors seran el seu nom i nacionalitat. Com és natural, hi ha obres d'art de les que es desconeix l'autor.
Dels museus recollirà la següent informació: el nom del museu, direcció amb el nom del carrer i el número, a més de la ciutat i el país on està.

Nota: establiu les claus primàries que considereu oportunes.

---

### Publicitat (versió curta)


Si voleu implementar una base de dades que almacene informació sobre la publicitat actualment emesa en els mitjans principals de comunicació. Hi ha tres tipus de suports publicitaris: televisió, ràdio i premsa escrita. De los anuncios se conoce el lema que utilitzen. Si l'anunci és televisiu o és una canya radiofónica, si voleu conèixer alguns minuts de durada, el nombre de les cadenes de televisió o emissores de ràdio respectivament que emeten i quantes vegades al dia s'emeten en cada un dels mitjans. Si l'anunci està imprès s'emmagatzemarà el nom i la tirada de les publicacions. De los anuncios impresos podran tener imagen o no . Un anunci pertany a una campanya publicitària que pot incloure altres anuncis. Cada campanya publicitària té un tema (venta d'un producte, promoció del turisme en una zona determinada, ajuda a determinats països, prevenció de malalties,...) i un pressupost total per a tots els anuncis que inclouen els mateixos. Dicha campanya publicitària la contracta un anunciant del que coneixem el seu nom i si és institució o empresa.

Finalmente se quiere contemplar cuáles de los medios audiovisuales (es decir cadenas de televisión y emisoras de ràdio) considerades abans son a les seves empreses que s'anunciïn.

Nota: establertes les claus primàries que consideren oportunitats.


---

### BIBLIOTECA

Es tracta d'informar un procés manual d'obtenció dels préstecs dels llibres d'una biblioteca. Actualment tenen fitxes de dos tipus:
* fitxes on se reconeixen les característiques dels llibres Títol, Autor/es, Editorial, Any de publicació,
* fitxes relatives als préstecs que s'han efectuat , títol del llibre, la persona a la qual se li ha prestat, la data de préstec i la devolució.
A més d'aquestes fitxes s'ha recabat informació sobre el sistema desitjat mitjançant el conjunt d'entrevistes amb els usuaris que es poden resumir de la següent forma:
Per als llibres interessats saber, a més del que apareix actualment a les fitxes, l'idioma en el que estan escrits. A més, voldreu fer cerques en funció de paraules clau, de forma que podrem trobar un llibre a través de diferents paraules clau. Per exemple, el llibre Sistemes i bases de dades seria un dels llibres que ens saldria a la consulta per paraula clau “model semàntic”.
Cada ejemplar del llibre tindrà un codi que serà únic i, a més, es reconeixerà característiques pròpies (té la tapa rota, fa falta encuadernar, etc). Cada llibre tractarà d'un o diversos temes, el que interessa reflectir per poder realitzar consultes del tipus “Libros o artículos que tenemos acerca de Bases de datos Multimedia”, “Artículos que podemos consultar sobre el lenguaje QUEL”, etc.
Los temas se pueden dividir en subtemas y así sucesivamente (no se sabe en cuantos niveles). Per exemple, en el tema de DISEÑO podem distingir una sèrie de subtemas, com DISEÑO CONCEPTUAL, DISEÑO LÓGICO, DISEÑO FÍSICO, DISEÑO GRÁFICO, DISEÑO ASISTIDO POR ORDENADOR, etc.Cada tema tindrà un nom i una descripció.
De los autors, a més del nom, ens interessa conèixer la seva nacionalitat. De les editorials emmagatzemarem el seu nom , direcció, ciutat i país.
A la biblioteca volem distingir tres tipus de socis: els alumnes, a les que a sumo se'ls oferirà una obra durant tres dies, alumnes de doctorat o projectes de final de carrera, que tindran accés com a màxim a dues obres durant la setmana i els professors y otras bibliotecas, a los que se les deixaran tres obres durant un termini màxim d'un mes.
De cada un d'ells emmagatzemen el seu DNI, Nombre, Direcció, Telèfon, Titulació en la que estan estudiant o impartint classes.
Els codis seran els propis del nostre sistema, excepte el DNI i l'ISBN. Se tiene que contemplar la mayor cantidad de información posible (lo más cercano a la realidad).
Proponer un esquema lògic així com el seu diagrama referencial

---

### Anuncis publicitat  (versió llarga)

Es proposa dissenyar una BD per millorar el control dels "spots" (anuncis) per a la televisió, i de tot el seu entorn, com són els canals de TV, franges horàries, agències de publicitat, tipus de productes que s'anuncien etc.
Al país hi ha diversos ens televisius. Es vol reflectir que uns són de titularitat pública, per exemple CCMA (Corporació Catalana de Mitjans Audiovisuals) i RTVE (Ràdio-Televisió Espanyola) i altres són de titularitat privada com Atresmedia. Aquests ens televisius disposen d'un o més canals d'emissió. Per exemple, CCMA disposa de TV3, El 33, Canal Super3, etc. RTVE disposa de La 1, La 2, 24 hores, Tododeporte, etc. Atresmedia disposa de La Sexta, Antena 3, etc. Cada canal s'identificarà sempre per un codi, tindrà un nom i una descripció.
Tots els "espots" suposarem que s'identifiquen per un codi assignat per una suposada "Oficina de Mitjans de Comunicació". Ha de considerar l'existència de "spots" equivalents, els quals podrien ser aquells que tenen les mateixes imatges però tenen diferent idioma.
Cada "espot" fa referència a un (el normal) o més (excepcionalment) tipus de productes (pensar en rentadores i detergents), i es vol tenir constància d'aquestes referències. Fins i tot s'ha de pensar que hi ha tipificats certs tipus de productes, independentment de si hi ha o no, en aquest moment, espots que facin referència a ells. Els "spots" són sempre propietat d'una única firma comercial, que és la que els paga, fins i tot en el cas d'anunciar més d'un tipus de producte.
Els "spots" són filmats, normalment, per una agència de publicitat encara que, en alguns casos, és la mateixa firma comercial que, amb mitjans propis, els produeix sense intervenció de cap agència publicitària.
Tal com s'ha indicat a l'principi, interessa també conèixer l'emissió dels "spots" en els diversos canals televisius. A efectes d'audiència, les 24 hores del dia no s'entenen com a tals hores, sinó com a "franges horàries" (com poden ser: matí, migdia, tarda, nit, ...) perfectament diferenciades. És més, el preu vigent (únic que interessa) de l'emissió en cada canal, depèn de la seva franja horària. Per simplificar el cas, farem la hipòtesi que el preu no depèn del dia de la setmana. Es vol tenir constància de l'nombre de vegades que cada "espot" s'ha emès en els diferents canals, la seva data d'emissió i la seva corresponent franja horària.
Finalment, hi ha un aspecte legal molt important a considerar. Hi ha alguns tipus de productes (realment molt pocs) que no estan permesos anunciar en certes franges horàries. L'incompliment serà penalitzat amb multes de gravetat qualificada d'1 a 3. Interessarà tenir constància de les prohibicions legals anteriors.

##### Opcional:  

De les agències de publicitat interessa conèixer el nom de el director artístic. De les firmes comercials interessa conèixer el nom de el cap de màrqueting. Però d'unes i altres són tipus d'empreses, de les quals interessa conèixer el CIF (codi d'identificació fiscal), nom, adreça i telèfon.
Les agències de publicitat disposen de directors cinematogràfics per filmar els "espots". Interessa conèixer l'historial de la contractació per part de les diferents agències d'aquests directors. Un director treballa, en un moment donat, per a una sola agència. Les agències volen conèixer de cada "spot" que director l'ha dirigit. Dels "espots" produïts directament per les firmes comercials, no interessa ni es vol tenir constància del seu director.
